---
Name: Sentry
Date: 2023-01-19
Type: Observability
Tags: observability, monitoring
Description: Open-source error tracking tool for monitoring and fixing crashes in real time.
---

### Overview

Sentry is a service that monitors and fix crashes in realtime. It provides an API for sending events from multiple languages in a range of applications. Sentry can triage, reproduce, and resolve errors efficiently and visibly.


### Commands



### Useful Snippets


### Further Resources

- [Sentry docs](https://docs.sentry.io/)