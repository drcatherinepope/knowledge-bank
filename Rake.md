---
Name: Rake
Date: 2023-01-27
Type: 
Tags: Ruby
Description: A task runner in Ruby.
---

### Overview

Performs tasks including:

- Making a database backup
- Running tests
- Gathering and reporting stats

### Commands


### Useful Snippets

Here's a simple Rake task:

```Ruby
desc "Print reminder about eating more fruit."

task :apple do
  puts "Eat more apples!"
end
```
To run this task:

```shell
rake apple

# "Eat more apples!"
```

### Further Resources