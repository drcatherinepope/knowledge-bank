---
Name: GLab
Date: 2023-01-03
Type: CLI
Tags: GitLab, CLI
Description: CLI tool for working with issues, merge requests, and pipelines.
---

### Overview

CLI tool for working with issues, merge requests, and pipelines. Available for repositories hosted on GitLab.com and self-managed GitLab instances.

### Setup

- Install with Homebrew: `brew install glab`
- Update with `brew upgrade glab`

To authenticate, you need to create a GitLab personal access token. You can then add this token to 1Password and authenticate with TouchID. This involves installing the 1Password CLI and linking it with your 1Password account.

### Useful Commands

`glab <command> <subcommand> [flags]`

`glab --help`

-   List merge requests assigned to you: `glab mr list --assignee=@me`
-   List review requests for you: `glab mr list --reviewer=@me`
-   Approve a merge request: `glab mr approve 235`
-   Create an issue and add milestone, title, and label: `glab issue create -m release-2.0.0 -t "My title here" --label important`


### Further Resources

- [Official Docs](https://gitlab.com/gitlab-org/cli)
- [Installation Instructions](https://gitlab.com/gitlab-org/cli#installation)