---
Name: Terraform
Date: 2023-01-03
Type: IaC
Tags: infrastructure, automation, cloud, CLI
Description: Open source infrastructure as code software tool that enables you to safely and predictably create, change, and improve infrastructure.
---

### Overview

Terraform is an open-source infrastructure-as-code software tool created by HashiCorp. Users define and provide data center infrastructure using a declarative configuration language known as HashiCorp Configuration Language, or optionally JSON.

### Setup

Homebrew:

- `brew tap hashicorp/tap`
- `brew install hashicorp/tap/terraform`
- 

### Useful Commands

`terraform [global options] <subcommand> [args]`

`terraform -help`

- `terraform init` - Prepare your working directory for other commands
- `terraform validate` - Check whether the configuration is valid
- `terraform plan` - Show changes required by the current configuration
- `terraform apply` - Create or update infrastructure destroy 
- `terraform destroy` - Destroy previously-created infrastructure

### Useful Snippets


### Further Resources