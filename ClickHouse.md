---
Name: ClickHouse
Date: 2023-01-04
Type: Database
Tags: observability
Description: Open source column-oriented DBMS for online analytical processing
---

### Overview

[ClickHouse](https://clickhouse.com/) is an open-source column-oriented DBMS for online analytical processing that allows users to generate analytical reports using SQL queries in real-time.

Observability and analytics features have big data and insert heavy requirements which are not a good fit for Postgres or Redis. ClickHouse was selected as a good fit to meet these features requirements.

### Further Resources