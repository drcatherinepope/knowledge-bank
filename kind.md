---
Name: kind
Date: 2023-01-03
Type: 
Tags: Kubernetes
Description: Tool for running local Kubernetes cluster using Docker for nodes.
---

### Overview

[kind](https://sigs.k8s.io/kind) is a tool for running local Kubernetes clusters using Docker container “nodes”.  kind was primarily designed for testing Kubernetes itself, but may be used for local development or CI.

kind comprises:

-   Go packages implementing cluster creation, image build, etc.
-   A command line interface (`kind`) built on these packages.
-   Docker image(s) written to run systemd, Kubernetes, etc.

kind bootstraps each "node" with kubeadm.

### Setup

Homebrew: `brew install kind`

You need go and Docker installed.

### Useful Commands

Create cluster called *testing*: `kind create cluster — name testing`

Create cluster and wait for all the components to be ready:  `kind create cluster — wait 2m`

Get running clusters: `kind get clusters`

Delete cluster: `kind delete cluster - name <cluster-name>`

### Useful Snippets


### Further Resources

- [Official Docs](https://kind.sigs.k8s.io/)