---
Name: asdf
Date: 2023-01-27
Type: 
Tags: CLI
Description: Manage multiple runtime versions with a single CLI tool and command interface.
---

### Overview

`asdf` is a tool version manager. All tool version definitions are contained within one file (`.tool-versions`) which you can check in to your project's Git repository to share with your team, ensuring everyone is using the **exact** same versions of tools.

The old way of working required multiple CLI version managers, each with their distinct API, configurations files and implementation (e.g. `$PATH` manipulation, shims, environment variables, etc...). `asdf` provides a single interface and configuration file to simplify development workflows, and can be extended to all tools and runtimes via a simple plugin interface.

Once `asdf` core is set up with your Shell configuration, plugins are installed to manage particular tools. When a tool is installed by a plugin, the executables that are installed have shims created for each of them. When you try and run one of these executables, the shim is run instead, allowing `asdf` to identify which version of the tool is set in `.tool-versions` and execute that version.

### Commands

This works if I'm getting an asdf-related error message:`rm -rf ~/.asdf/shims && asdf reshim`

### Useful Snippets


### Further Resources

[Official docs](https://asdf-vm.com/guide/getting-started.html)