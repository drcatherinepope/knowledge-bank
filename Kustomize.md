---
Name: Kustomize
Date: 2023-01-16
Type: tool
Tags: Kubernetes
Description: Kustomize lets you reuse the same base configuration to manage multiple different configurations.
---

### Overview

With Kustomize, you can use one base file across several different environments (for example, development, staging, production), with minimal, unique overlays to customize the base for each environment.

### Commands


### Useful Snippets


### Further Resources

- [Official Docs](https://kubectl.docs.kubernetes.io/)
