---
Name: Docker
Date: 2023-01-03
Type: 
Description: Makes it easy to build, run, and manage containers.
---

### Overview

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers.

### Setup

### Useful Commands

Open Docker Desktop from the terminal - `open -a Docker`

`docker ps`

### Useful Snippets


### Further Resources