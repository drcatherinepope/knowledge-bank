---
Name: kubectl
Date: 2023-01-03
Type: CLI
Tags: Kubernetes, CLI
Description: "Command line tool for communicating with a Kubernetes cluster's control plane"
---

### Overview

The Kubernetes command-line tool, [kubectl](https://kubernetes.io/docs/reference/kubectl/kubectl/), allows you to run commands against Kubernetes clusters. You can use kubectl to deploy applications, inspect and manage cluster resources, and view logs.

### Setup

- Brew: `brew install kubectl`
- Check it's up-to-date:  `kubectl version --client`


### Useful Commands

`kubectl [command] [TYPE] [NAME] [flags]`

`kubectl get all`

#### Pods

`kubectl get pods`
`kubectl get pod <pod-name>`

#### Services

`kubectl get services`


### Useful Snippets


### Further Resources

- [kubectl Cheat Sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- 